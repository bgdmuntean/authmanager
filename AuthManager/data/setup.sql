-- Create Database if not exists
IF NOT EXISTS(SELECT * FROM sys.databases WHERE name = 'AuthLogs')
  BEGIN
    CREATE DATABASE [AuthLogs]
  END
GO

-- Use database 
USE [AuthLogs]
GO


-- Create tables if not exists

-- Application
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'App' AND xtype = 'U')
		CREATE TABLE [App] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
			[Label] NVARCHAR(128) NOT NULL,
            [IsEnabled] BIT NOT NULL DEFAULT 0,
			[IsDeleted] BIT NOT NULL DEFAULT 0,
			[CreatedAt] DATETIMEOFFSET NOT NULL,
			[ModifiedAt] DATETIMEOFFSET NOT NULL,
		)

-- Log
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'Log' AND xtype = 'U')
		CREATE TABLE [Log] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
			[Operation] NVARCHAR(128) NOT NULL,
            [Status] NVARCHAR(64) NOT NULL,
            [Message] NVARCHAR(64) NOT NULL,
			[CreatedAt] DATETIMEOFFSET NOT NULL,
			[AppId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES [App](Id)  NOT NULL
		)

-- ApiKey
IF NOT EXISTS (
	SELECT * 
	FROM sysobjects 
	WHERE NAME = 'ApiKey' AND xtype = 'U')
		CREATE TABLE [ApiKey] (
			[Id] UNIQUEIDENTIFIER PRIMARY KEY DEFAULT NEWID(),
			[Value] NVARCHAR(128) NOT NULL,
            [IsEnabled] BIT NOT NULL DEFAULT 0,
			[IsDeleted] BIT NOT NULL DEFAULT 0,
			[CreatedAt] DATETIMEOFFSET NOT NULL,
			[ModifiedAt] DATETIMEOFFSET NOT NULL,
			[AppId] UNIQUEIDENTIFIER FOREIGN KEY REFERENCES [App](Id)  NOT NULL
		)

GO