﻿using AuthManager.API.Entities;
using AuthManager.Domain.Models.Redis;
using AuthManager.Infrastructure.Models.Data;
using AuthManager.Infrastructure.Models.Redis;
using AutoMapper;

namespace AuthManager.API.Utils.Configurations
{
    public class MappingProfiles : Profile
    {
        public MappingProfiles()
        {
            CreateMap<ApiKey, ApiKeyDto>();
            CreateMap<ApiKeyDto, ApiKey>();

            CreateMap<App, AppDto>();
            CreateMap<AppDto, App>();

            CreateMap<Log, LogDto>();
            CreateMap<LogDto, Log>();

            CreateMap<RedisSecret, RedisSecretDto>();
            CreateMap<RedisSecretDto, RedisSecret>();
        }
    }
}
