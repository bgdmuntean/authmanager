﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AuthManager.API.Entities
{
    public partial class Application
    {
        public Application()
        {
            ApiKeys = new HashSet<ApiKey>();
            Logs = new HashSet<Log>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public virtual ICollection<ApiKey> ApiKeys { get; set; }
        public virtual ICollection<Log> Logs { get; set; }
    }
}
