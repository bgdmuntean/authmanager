﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AuthManager.API.Entities
{
    public partial class Log
    {
        public Guid Id { get; set; }
        public string Operation { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public Guid AppId { get; set; }

        public virtual App App { get; set; }
    }
}
