﻿using System;
using System.Collections.Generic;

#nullable disable

namespace AuthManager.API.Entities
{
    public partial class ApiKey
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public Guid AppId { get; set; }

        public virtual App App { get; set; }
    }
}
