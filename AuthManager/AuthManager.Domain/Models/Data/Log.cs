﻿using System;


namespace AuthManager.Domain.Models.Data
{
    public sealed class Log
    {
        public Guid Id { get; set; }
        public string Operation { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public Guid ApplicationId { get; set; }
    }
}
