﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public sealed class App
    {
        public Guid Id { get; set; }
        public string Label { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
    }
}
