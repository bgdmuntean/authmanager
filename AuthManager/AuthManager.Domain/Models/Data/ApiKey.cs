﻿using System;

namespace AuthManager.Domain.Models.Data
{
    public sealed class ApiKey
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public Guid ApplicationId { get; set; }
    }
}
