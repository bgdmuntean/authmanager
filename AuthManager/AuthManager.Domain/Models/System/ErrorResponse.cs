﻿using System.Net;
using Newtonsoft.Json;

namespace AuthManager.Domain.Models.System
{
    public class ErrorResponse
    {
        public HttpStatusCode StatusCode { get; set; } = HttpStatusCode.InternalServerError;
        public string Message { get; set; } = "An error has occurred on the server side";


        public string ToJsonString()
            => JsonConvert.SerializeObject(this);
    }
}
