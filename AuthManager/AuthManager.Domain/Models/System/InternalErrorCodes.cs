﻿namespace AuthManager.Domain.Models.System
{
    public enum InternalErrorCodes
    {
        InternalError = 100001,
        UnauthorizedCallError = 100002,
        TimeOutError = 100003,
        DatabaseEntityNotFound = 100004,
        DatabaseOperationForbidden = 100005,
        DatabaseOperationConflict = 100006,
    }
}