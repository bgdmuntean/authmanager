﻿namespace AuthManager.Domain.Models.System
{
    public enum DatabaseOperation
    {
        Create,
        Read,
        Update,
        Delete
    }
}
