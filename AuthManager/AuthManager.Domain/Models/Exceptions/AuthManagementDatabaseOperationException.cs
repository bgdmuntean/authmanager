﻿using System.Net;
using AuthManager.Domain.Models.System;

namespace AuthManager.Domain.Models.Exceptions
{
    public class AuthManagementDatabaseOperationException : AuthManagementException
    {
        public DatabaseOperation Operation { get; set; }

        public AuthManagementDatabaseOperationException(
            HttpStatusCode statusCode,
            InternalErrorCodes errorCode,
            string message, 
            DatabaseOperation operation) : base(statusCode, errorCode, message)
        {
            Operation = operation;
        }
    }
}