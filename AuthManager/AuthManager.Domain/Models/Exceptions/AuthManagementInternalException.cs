﻿using System;
using System.Net;
using AuthManager.Domain.Models.System;

namespace AuthManager.Domain.Models.Exceptions
{
    public sealed class AuthManagementInternalException : AuthManagementException
    {
        public Exception InternalException { get; set; }

        public AuthManagementInternalException(string message, Exception ex): base(HttpStatusCode.InternalServerError, InternalErrorCodes.InternalError, message)
        {
            InternalException = ex;
        }

        public AuthManagementInternalException(string message) : base(HttpStatusCode.InternalServerError, InternalErrorCodes.InternalError, message)
        {
        }
    }
}
