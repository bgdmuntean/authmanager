﻿using System;
using System.Net;
using AuthManager.Domain.Models.System;

namespace AuthManager.Domain.Models.Exceptions
{
    public abstract class AuthManagementException : Exception
    {
        public HttpStatusCode StatusCode { get; set; }
        public InternalErrorCodes ErrorCode { get; set; }

        protected AuthManagementException(HttpStatusCode statusCode, InternalErrorCodes errorCode, string message) : base(message)
        {
            StatusCode = statusCode;
            ErrorCode = errorCode;
        }

        protected AuthManagementException(HttpStatusCode statusCode, string message) : base(message)
        {
            StatusCode = statusCode;
            ErrorCode = InternalErrorCodes.InternalError;
        }

        protected AuthManagementException(string message) : base(message)
        {
            StatusCode = HttpStatusCode.InternalServerError;
            ErrorCode = InternalErrorCodes.InternalError;
        }

        protected AuthManagementException() : base()
        {

        }
    }
}
