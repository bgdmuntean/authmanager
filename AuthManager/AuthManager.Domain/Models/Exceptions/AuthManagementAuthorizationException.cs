﻿using System.Net;
using AuthManager.Domain.Models.System;

namespace AuthManager.Domain.Models.Exceptions
{
    public sealed class AuthManagementAuthorizationException : AuthManagementException
    {
        public AuthManagementAuthorizationException(string message):base(HttpStatusCode.Unauthorized, InternalErrorCodes.UnauthorizedCallError, message)
        {
            
        }
    }
}
