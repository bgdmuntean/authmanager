﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Application.Models.Contracts.v1.Filters;
using AuthManager.Domain.Models.Data;

namespace AuthManager.Application.Interfaces.v1.ApiKeys
{
    public interface IApiKeyRepository
    {
        Task<ApiKey> CreateApiKeyAsync(string value, Guid applicationId);
        Task<ApiKey> GetApiKeyAsync(Guid apiKeyId);
        Task<ApiKey> GetApiKeyAsync(string apiKey);
        Task<IEnumerable<ApiKey>> GetApiKeysByApplicationIdAsync(Guid applicationId, ApiKeysFilter filter, PaginationOptions paginationOptions);
        Task<IEnumerable<ApiKey>> GetApiKeysAsync(ApiKeysFilter filter, PaginationOptions paginationOptions);
        Task UpdateApiKeyAsync(Guid id, string value, bool isEnabled, bool isDeleted);
        Task EnableApiKeyAsync(Guid apiKeyId);
        Task DisableApiKeyAsync(Guid apiKeyId);
        Task DeleteApiKeyAsync(Guid apiKeyId);
    }
}