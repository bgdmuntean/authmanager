﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Application.Models.Contracts.v1.Filters;
using AuthManager.Domain.Models.Data;

namespace AuthManager.Application.Interfaces.v1.Apps
{
    public interface IAppRepository
    {
        Task<App> CreateAppAsync(string label);
        Task<App> GetAppAsync(Guid appId);
        Task<App> GetAppAsync(string appLabel);
        Task<IEnumerable<App>> GetAppsAsync(AppsFilter filter, PaginationOptions paginationOptions);
        Task UpdateAppAsync(Guid id, string label, bool isEnabled, bool isDeleted);
        Task EnableAppAsync(Guid appId);
        Task DisableAppAsync(Guid appId);
        Task DeleteAppAsync(Guid appId);
    }
}