﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Domain.Models.Data;

namespace AuthManager.Application.Interfaces.v1.Logs
{
    public interface ILogRepository
    {
        Task<Log> CreateLogAsync(string operation, string status, string message, Guid appId);
        Task<Log> GetLogAsync(Guid logId);
        Task<IEnumerable<Log>> GetLogsForAppAsync(Guid appId, PaginationOptions paginationOptions);
        Task<IEnumerable<Log>> GetLogsAsync(PaginationOptions paginationOptions);
        Task DeleteLogAsync(Guid logId);
        Task DeleteLogsForAppAsync(Guid appId);
    }
}