﻿using System;

namespace AuthManager.Application.Models.Contracts.v1.Filters
{
    public class AppsFilter
    {
        public bool Everything { get; set; } = false;
        public bool IsEnabled { get; set; } = true;
        public bool IsDeleted { get; set; } = false;
        public DateTimeOffset? CreatedAfter { get; set; }
        public DateTimeOffset? CreatedBefore { get; set; }
    }
}
