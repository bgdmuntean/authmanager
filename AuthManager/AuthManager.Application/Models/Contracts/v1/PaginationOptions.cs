﻿namespace AuthManager.Application.Models.Contracts.v1
{
    public sealed class PaginationOptions
    {
        public int Page { get; set; } = 1;
        public int PageSize { get; set; } = 50;
    }
}
