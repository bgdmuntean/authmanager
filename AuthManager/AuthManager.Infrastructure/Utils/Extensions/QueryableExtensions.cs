﻿using System.Linq;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Application.Models.Contracts.v1.Filters;
using AuthManager.Infrastructure.Models.Data;

namespace AuthManager.Infrastructure.Utils.Extensions
{
    public static class QueryableExtensions
    {
        public static IQueryable<ApiKeyDto> ApplyFiltering(this IQueryable<ApiKeyDto> queryable, ApiKeysFilter filter)
        {
            if (!filter.Everything)
            {
                queryable = queryable.Where(_ => _.IsDeleted && _.IsEnabled);
            }

            if (filter.CreatedAfter != null)
            {
                queryable = queryable.Where(_ => _.CreatedAt > filter.CreatedAfter);
            }

            if (filter.CreatedBefore != null)
            {
                queryable = queryable.Where(_ => _.CreatedAt < filter.CreatedBefore);
            }

            return queryable;
        }

        public static IQueryable<AppDto> ApplyFiltering(this IQueryable<AppDto> queryable, AppsFilter filter)
        {
            if (!filter.Everything)
            {
                queryable = queryable.Where(_ => _.IsDeleted && _.IsEnabled);
            }

            if (filter.CreatedAfter != null)
            {
                queryable = queryable.Where(_ => _.CreatedAt > filter.CreatedAfter);
            }

            if (filter.CreatedBefore != null)
            {
                queryable = queryable.Where(_ => _.CreatedAt < filter.CreatedBefore);
            }

            return queryable;
        }

        public static IQueryable<T> ApplyPagination<T>(this IQueryable<T> queryable, PaginationOptions pagination)
        {
            return queryable
                .Skip(pagination.Page - 1 * pagination.PageSize)
                .Take(pagination.PageSize);
        }
    }
}