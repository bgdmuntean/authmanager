﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using AuthManager.Application.Interfaces.v1.Apps;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Application.Models.Contracts.v1.Filters;
using AuthManager.Domain.Models.Data;
using AuthManager.Domain.Models.Exceptions;
using AuthManager.Domain.Models.System;
using AuthManager.Infrastructure.Models.Data;
using AuthManager.Infrastructure.Utils.Extensions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AuthManager.Infrastructure.Repositories.v1.Apps
{
    internal class AppRepository : IAppRepository
    {
        private readonly AuthLogsContext _dbContext;
        private readonly IMapper _mapper;

        public AppRepository(AuthLogsContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public async Task<App> CreateAppAsync(string label)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => string.Equals(label, _.Label, StringComparison.InvariantCultureIgnoreCase))
                .ConfigureAwait(false);

            if (app != null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Conflict,
                    InternalErrorCodes.DatabaseOperationConflict,
                    $"Create operation failed. Reason: [App] with label [{label}] already exists.",
                    DatabaseOperation.Create);

            app = new AppDto
            {
                CreatedAt = DateTimeOffset.Now,
                ModifiedAt = DateTimeOffset.Now,
                IsDeleted = false,
                IsEnabled = true,
                Label = label
            };

            await _dbContext.AddAsync(app).ConfigureAwait(false);
            await _dbContext.SaveChangesAsync().ConfigureAwait(false);

            return _mapper.Map<App>(app);
        }

        public async Task<App> GetAppAsync(Guid appId)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => _.Id == appId)
                .ConfigureAwait(false);

            if (app == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Read operation failed. Reason: [App] with id [{appId}] was not found.",
                    DatabaseOperation.Read);

            return _mapper.Map<App>(app);
        }

        public async Task<App> GetAppAsync(string appLabel)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => string.Equals(_.Label, appLabel, StringComparison.InvariantCultureIgnoreCase))
                .ConfigureAwait(false);

            if (app == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Read operation failed. Reason: [App] with label [{appLabel}] was not found.",
                    DatabaseOperation.Read);

            return _mapper.Map<App>(app);
        }

        public async Task<IEnumerable<App>> GetAppsAsync(AppsFilter filter, PaginationOptions paginationOptions)
        {
            var appQueryable = _dbContext
                .Apps
                .AsNoTracking();

            appQueryable = appQueryable.ApplyFiltering(filter);
            appQueryable = appQueryable.ApplyPagination(paginationOptions);

            return _mapper.Map<App[]>(await appQueryable.ToArrayAsync().ConfigureAwait(false));
        }

        public async Task UpdateAppAsync(Guid id, string label, bool isEnabled, bool isDeleted)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => _.Id == id)
                .ConfigureAwait(false);

            if (app == null)
                throw new AuthManagementDatabaseOperationException(HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Update operation failed. Reason: [App] with id [{id}] was not found.",
                    DatabaseOperation.Update);

            app.IsEnabled = isEnabled;
            app.IsDeleted = isDeleted;
            app.Label = label;
            app.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task EnableAppAsync(Guid appId)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => _.Id == appId)
                .ConfigureAwait(false);

            if (app == null)
                throw new AuthManagementDatabaseOperationException(HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Enable operation failed. Reason: [App] with id [{appId}] was not found.",
                    DatabaseOperation.Update);

            if (app.IsDeleted)
            {
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Forbidden,
                    InternalErrorCodes.DatabaseOperationForbidden,
                    $"Enable operation failed. Reason: [App] with id [{appId}] is marked as deleted.",
                    DatabaseOperation.Update);
            }

            app.IsEnabled = true;
            app.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DisableAppAsync(Guid appId)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => _.Id == appId)
                .ConfigureAwait(false);

            if(app == null)
                throw new AuthManagementDatabaseOperationException(HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Disable operation failed. Reason: [App] with id [{appId}] was not found.",
                    DatabaseOperation.Update);

            if (app.IsDeleted)
            {
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Forbidden,
                    InternalErrorCodes.DatabaseOperationForbidden,
                    $"Disable operation failed. Reason: [App] with id [{appId}] is marked as deleted.",
                    DatabaseOperation.Update);
            }

            app.IsEnabled = false;
            app.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteAppAsync(Guid appId)
        {
            var app = await _dbContext.Apps
                .FirstOrDefaultAsync(_ => _.Id == appId)
                .ConfigureAwait(false);

            if(app == null)
                throw new AuthManagementDatabaseOperationException(HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Delete operation failed. Reason: [App] with id [{appId}] was not found.",
                    DatabaseOperation.Delete);

            app.IsEnabled = false;
            app.IsDeleted = true;
            app.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}