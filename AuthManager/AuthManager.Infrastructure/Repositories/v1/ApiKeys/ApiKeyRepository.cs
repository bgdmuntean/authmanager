﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using AuthManager.Application.Interfaces.v1.ApiKeys;
using AuthManager.Application.Models.Contracts.v1;
using AuthManager.Application.Models.Contracts.v1.Filters;
using AuthManager.Domain.Models.Data;
using AuthManager.Domain.Models.Exceptions;
using AuthManager.Domain.Models.System;
using AuthManager.Infrastructure.Models.Data;
using AuthManager.Infrastructure.Utils.Extensions;
using AutoMapper;
using Microsoft.EntityFrameworkCore;

namespace AuthManager.Infrastructure.Repositories.v1.ApiKeys
{
    internal class ApiKeyRepository : IApiKeyRepository
    {
        private readonly AuthLogsContext _dbContext;
        private readonly IMapper _mapper;

        public ApiKeyRepository(AuthLogsContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }


        public async Task<ApiKey> CreateApiKeyAsync(string value, Guid applicationId)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => string.Equals(value, _.Value) && applicationId == _.AppId)
                .ConfigureAwait(false);

            if (apiKey != null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Conflict,
                    InternalErrorCodes.DatabaseOperationConflict,
                    $"Create operation failed. Reason: [ApiKey] with ApplicationId [{applicationId}] and Value [{value}] already exists.",
                    DatabaseOperation.Create);

            apiKey = new ApiKeyDto
            {
                AppId = applicationId,
                CreatedAt = DateTimeOffset.Now,
                ModifiedAt = DateTimeOffset.Now,
                IsDeleted = false,
                IsEnabled = true,
                Value = value
            };

            await _dbContext.AddAsync(apiKey).ConfigureAwait(false);
            await _dbContext.SaveChangesAsync().ConfigureAwait(false);

            return _mapper.Map<ApiKey>(apiKey);
        }

        public async Task<ApiKey> GetApiKeyAsync(Guid apiKeyId)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => apiKeyId == _.Id)
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Read operation failed. Reason: [ApiKey] with id [{apiKeyId}] was not found",
                    DatabaseOperation.Read);

            return _mapper.Map<ApiKey>(apiKey);
        }

        public async Task<ApiKey> GetApiKeyAsync(string apiKeyValue)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => string.Equals(_.Value, apiKeyValue, StringComparison.InvariantCultureIgnoreCase))
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Read operation failed. Reason: [ApiKey] with value [{apiKeyValue}] was not found",
                    DatabaseOperation.Read);

            return _mapper.Map<ApiKey>(apiKey);
        }

        public async Task<IEnumerable<ApiKey>> GetApiKeysByApplicationIdAsync(Guid applicationId, ApiKeysFilter filter, PaginationOptions paginationOptions)
        {
            var apiKeyQuery = _dbContext
                .ApiKeys
                .AsNoTracking();

            apiKeyQuery = apiKeyQuery
                .Where(_ => _.AppId == applicationId);

            apiKeyQuery = apiKeyQuery.ApplyFiltering(filter);
            apiKeyQuery = apiKeyQuery.ApplyPagination(paginationOptions);

            return _mapper.Map<ApiKey[]>(await apiKeyQuery.ToArrayAsync());
        }

        public async Task<IEnumerable<ApiKey>> GetApiKeysAsync(ApiKeysFilter filter, PaginationOptions paginationOptions)
        {
            var apiKeyQuery = _dbContext
                .ApiKeys
                .AsNoTracking();

            apiKeyQuery = apiKeyQuery.ApplyFiltering(filter);
            apiKeyQuery = apiKeyQuery.ApplyPagination(paginationOptions);

            return _mapper.Map<ApiKey[]>(await apiKeyQuery.ToArrayAsync());
        }

        public async Task UpdateApiKeyAsync(Guid id, string value, bool isEnabled, bool isDeleted)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => id == _.Id)
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Update operation failed. Reason: [ApiKey] with id [{id}] was not found.",
                    DatabaseOperation.Update);

            apiKey.IsEnabled = isEnabled;
            apiKey.IsDeleted = isDeleted;
            apiKey.ModifiedAt = DateTimeOffset.Now;
            apiKey.Value = value;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task EnableApiKeyAsync(Guid apiKeyId)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => apiKeyId == _.Id)
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Enable operation failed. Reason: [ApiKey] with id [{apiKeyId}] was not found.",
                    DatabaseOperation.Update);

            if (apiKey.IsDeleted)
            {
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Forbidden,
                    InternalErrorCodes.DatabaseOperationForbidden,
                    $"Enable operation failed. Reason: [ApiKey] with id [{apiKeyId}] is marked as deleted.",
                    DatabaseOperation.Update);
            }

            apiKey.IsEnabled = false;
            apiKey.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DisableApiKeyAsync(Guid apiKeyId)
        {

            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => apiKeyId == _.Id)
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Disable operation failed. Reason: [ApiKey] with id [{apiKeyId}] was not found.",
                    DatabaseOperation.Update);

            if (apiKey.IsDeleted)
            {
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.Forbidden,
                    InternalErrorCodes.DatabaseOperationForbidden,
                    $"Disable operation failed. Reason: [ApiKey] with id [{apiKeyId}] is marked as deleted.",
                    DatabaseOperation.Update);
            }

            apiKey.IsEnabled = false;
            apiKey.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }

        public async Task DeleteApiKeyAsync(Guid apiKeyId)
        {
            var apiKey = await _dbContext.ApiKeys
                .FirstOrDefaultAsync(_ => apiKeyId == _.Id)
                .ConfigureAwait(false);

            if (apiKey == null)
                throw new AuthManagementDatabaseOperationException(
                    HttpStatusCode.NotFound,
                    InternalErrorCodes.DatabaseEntityNotFound,
                    $"Delete operation failed. Reason: [ApiKey] with id [{apiKeyId}] was not found.",
                    DatabaseOperation.Delete);

            apiKey.IsDeleted = true;
            apiKey.IsEnabled = false;
            apiKey.ModifiedAt = DateTimeOffset.Now;

            await _dbContext.SaveChangesAsync().ConfigureAwait(false);
        }
    }
}