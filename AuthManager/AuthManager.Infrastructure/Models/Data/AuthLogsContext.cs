﻿using Microsoft.EntityFrameworkCore;

namespace AuthManager.Infrastructure.Models.Data
{
    public sealed class AuthLogsContext : DbContext
    {
        public AuthLogsContext()
        {
        }

        public AuthLogsContext(DbContextOptions<AuthLogsContext> options) : base(options)
        {
        }

        public DbSet<ApiKeyDto> ApiKeys { get; set; }
        public DbSet<AppDto> Apps { get; set; }
        public DbSet<LogDto> Logs { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<ApiKeyDto>(entity =>
            {
                entity.ToTable("ApiKey");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Value)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.HasOne(d => d.App)
                    .WithMany(p => p.ApiKeys)
                    .HasForeignKey(d => d.AppId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__ApiKey__AppId__300424B4");
            });

            modelBuilder.Entity<AppDto>(entity =>
            {
                entity.ToTable("App");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Label)
                    .IsRequired()
                    .HasMaxLength(128);
            });

            modelBuilder.Entity<LogDto>(entity =>
            {
                entity.ToTable("Log");

                entity.Property(e => e.Id).HasDefaultValueSql("(newid())");

                entity.Property(e => e.Message)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.Property(e => e.Operation)
                    .IsRequired()
                    .HasMaxLength(128);

                entity.Property(e => e.Status)
                    .IsRequired()
                    .HasMaxLength(64);

                entity.HasOne(d => d.App)
                    .WithMany(p => p.Logs)
                    .HasForeignKey(d => d.AppId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__Log__AppId__2A4B4B5E");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        private void OnModelCreatingPartial(ModelBuilder modelBuilder)
        {
            throw new System.NotImplementedException();
        }
    }
}
