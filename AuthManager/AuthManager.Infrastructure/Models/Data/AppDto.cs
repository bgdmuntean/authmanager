﻿using System;
using System.Collections.Generic;

namespace AuthManager.Infrastructure.Models.Data
{
    public sealed class AppDto
    {
        public AppDto()
        {
            ApiKeys = new HashSet<ApiKeyDto>();
            Logs = new HashSet<LogDto>();
        }

        public Guid Id { get; set; }
        public string Label { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }

        public ICollection<ApiKeyDto> ApiKeys { get; set; }
        public ICollection<LogDto> Logs { get; set; }
    }
}
