﻿using System;

namespace AuthManager.Infrastructure.Models.Data
{
    public sealed class ApiKeyDto
    {
        public Guid Id { get; set; }
        public string Value { get; set; }
        public bool IsEnabled { get; set; }
        public bool IsDeleted { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public Guid AppId { get; set; }

        public AppDto App { get; set; }
    }
}
