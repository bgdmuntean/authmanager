﻿using System;

namespace AuthManager.Infrastructure.Models.Data
{
    public sealed class LogDto
    {
        public Guid Id { get; set; }
        public string Operation { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public Guid AppId { get; set; }

        public AppDto App { get; set; }
    }
}
