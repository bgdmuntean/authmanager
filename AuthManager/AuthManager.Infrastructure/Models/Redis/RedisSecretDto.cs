﻿using System;

namespace AuthManager.Infrastructure.Models.Redis
{
    public class RedisSecretDto
    {
        public string PrivateKey { get; set; }
        public string PublicKey { get; set; }
        public bool IsEnabled { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset ModifiedAt { get; set; }
        public string ApplicationIdentifier { get; set; }
    }
}